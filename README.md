# Discuz- Q - PC

#### 介绍
二次开发Discuz！Q前端界面，官方Gitee地址：https://gitee.com/Discuz/Discuz-Q-Web

#### 功能说明
均在红版基础上修改。
1. 增加首页轮播图，文件修改 components/WorkCarousel.vue 
2. 增加底部版权、小程序、公众号、备案信息，文件修改 components/BottomFooter.vue
3. 增加首页阅读量
4. 增加免责条款，文件修改 components/PostItemPay.vue
5. 增加Java后端

[查看部署文档](https://doc.work100.com.cn)



