/*
 Navicat Premium Data Transfer

 Source Server         : 49.232.152.121_6999
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : 49.232.152.121:6999
 Source Schema         : uw_bbs

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 19/04/2021 11:07:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for third_carousel
-- ----------------------------
DROP TABLE IF EXISTS `third_carousel`;
CREATE TABLE `third_carousel`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '轮播图url',
  `type` int NULL DEFAULT NULL COMMENT '轮播图类型：0三端显示，1PC，2H5，3小程序',
  `href` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转地址',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件存储路径',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
