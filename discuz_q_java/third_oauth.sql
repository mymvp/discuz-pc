/*
 Navicat Premium Data Transfer

 Source Server         : 49.232.152.121_6999
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : 49.232.152.121:6999
 Source Schema         : uw_bbs

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 19/04/2021 11:07:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for third_oauth
-- ----------------------------
DROP TABLE IF EXISTS `third_oauth`;
CREATE TABLE `third_oauth`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权类型',
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `uuid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权ID',
  `user_id` int NULL DEFAULT NULL COMMENT '绑定id',
  `create_time` datetime NULL DEFAULT NULL COMMENT '绑定时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
